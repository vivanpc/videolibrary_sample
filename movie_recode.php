<?php
	require_once 'movie_master_config.php';
?>
<!DOCTYPE html>
<html>
	<head>

		<script src="node_modules\jquery\dist\jquery.min.js"></script>
		<!-- <script src="node_modules\datatables\media\js\jquery.dataTables.min.js"></script> -->
		<script src="node_modules\semantic-ui\dist\semantic.min.js"></script>


		<!-- <link rel="stylesheet" type="text/css" href="css/videokiosk.css"> -->
		<link rel="stylesheet" type="text/css" href="css/movie_recode.css">
		<link rel="stylesheet" type="text/css" href="node_modules\semantic-ui\dist\semantic.min.css">

		<title>Movie Gallery</title>
		<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
	</head>
	<body id="movie_recode_body">
		<div style="text-align:center;">
			<h1>Recode your movie</h1>
		</div>

		<pre>
		<!-- <?php print_r( $_GET ); ?>

		<?php print_r( $_POST ); ?> -->

		</pre>

		<br/>

		<?php
			$mm = new movie_master_config;
			// $mm->Machine_Root, null, $mm->Aliased_Root

			//AAC audio " -acodec aac " is superior to mp3 codec
			//some files eg. '30 minutes or less' do not convert to browser playable audio unless codec is specified
			// recommed specifing aac codec
			// if possible to check first that would be ideal as '-acodec copy' can be used

			$input = str_replace( $mm->Aliased_Root, $mm->Machine_Root, $_POST['path']);

			$output = str_replace( $mm->Aliased_Root, $mm->Machine_Root, $_POST['path'].'.mp4');


			$acodec = "-acodec aac";

			$command = "ffmpeg -i \"".$input."\" ".$acodec." \"".$output."\"";
			
		?>
			<div class="command_line_entry"><?php print_r($command); ?></div>
		<?php

			// Optional run recode command here
			// somehow display ongoing progress through stream
			$test_command = "echo \"hello mars\"";

			// $output = shell_exec($test_command);
			// echo "<pre>$output</pre>";
			


		?>

		<div class="page_link">
			<a href="movie_gallery.php">Movie Gallery</a>
		</div>
	</body>
</html>

<?php

	

	// FROM STACKOVERFLOW or some other forum -- not yet tested by me
	/**
	 * Execute the given command by displaying console output live to the user.
	 *  @param  string  cmd          :  command to be executed
	 *  @return array   exit_status  :  exit status of the executed command
	 *                  output       :  console output of the executed command
	 */
	function liveExecuteCommand($cmd)
	{

	    while (@ ob_end_flush()); // end all output buffers if any

	    $proc = popen("$cmd 2>&1 ; echo Exit status : $?", 'r');

	    $live_output     = "";
	    $complete_output = "";

	    while (!feof($proc))
	    {
	        $live_output     = fread($proc, 4096);
	        $complete_output = $complete_output . $live_output;
	        echo "$live_output";
	        @ flush();
	    }

	    pclose($proc);

	    // get exit status
	    preg_match('/[0-9]+$/', $complete_output, $matches);

	    // return exit status and intended output
	    return array (
	                    'exit_status'  => intval($matches[0]),
	                    'output'       => str_replace("Exit status : " . $matches[0], '', $complete_output)
	                 );
	}

?>