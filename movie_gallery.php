<!DOCTYPE html>
<html>
	<head>

		<script src="node_modules\jquery\dist\jquery.min.js"></script>
		<script src="node_modules\datatables\media\js\jquery.dataTables.min.js"></script>
		<script src="node_modules\semantic-ui\dist\semantic.min.js"></script>


		<!-- <link rel="stylesheet" type="text/css" href="css/videokiosk.css"> -->
		<link rel="stylesheet" type="text/css" href="css/movie_gallery.css">
		<link rel="stylesheet" type="text/css" href="node_modules\semantic-ui\dist\semantic.min.css">


		<!-- <link rel="stylesheet" type="text/css" href="node_modules\datatables.net-se\css\dataTables.semanticui.min.css"> -->

		<title>Movie Gallery</title>
		<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
	</head>
	<body id="movie_gallery_body">
		<img id="main_nebula_gnome_logo" src='images/nebula_gnome.png'/>
		<div id="movie_player_outer_container">
			<div id="movie_player_inner_container">
				<video id="video_player" controls src="">
					Sorry. The browser you are using sucks for this.
				</video>
			</div>
		</div>

		<table id="movie_list_table" class="display">
			<thead>
				<tr>
					<th>Name</th>
					<th>Year</th>
					<th>Video</th>
					<th>Audio</th>
					<th>Files</th>
					<th>3D</th>

				</tr>
			</thead>
			<tbody>

				<?php

					$string = file_get_contents("movies.json");
					$movies = json_decode($string, true);

					foreach ($movies as $key => $movie) {
				?>
						<tr>
							<td>
								<?php
									//title name
									if( is_array($movie['title']) && count($movie['title']) > 0 && $movie['title'][0] !== "" ){
										foreach ($movie['title'] as $i_title => $title) {
											print_r("<b>TITLE:</b> ".$title."<br/>");
										}
									}

									if( is_array($movie['name']) && count($movie['name']) > 0 && $movie['name'] != "" ){
										foreach ($movie['name'] as $i_name => $name) {
											print_r($name."<br/>");
										}
									}

								?>
							</td>
							<td>
								<?php 
									//year
									if( is_array($movie['year']) ){
										foreach ($movie['year'] as $i_year => $year) {
											print_r($year."<br/>");
										}
									}
								?>
							</td>
							<td>
								<?php 
									//video
									if( is_array($movie['video']) ){
										foreach ($movie['video'] as $i_video => $video_quality) {
											print_r($video_quality."<br/>");
										}
									}
								?>
							</td>
							<td>
								<?php
									//audio
									if( is_array($movie['audio']) ){
										foreach ($movie['audio'] as $i_audio => $audio_quality) {
											print_r($audio_quality."<br/>");
										}
									}
								?>
							</td>

							<td>
								<div class="scroll">
									<?php
										//files
										if( is_array($movie['files']) ){
											foreach ($movie['files'] as $i_file => $file) {

												$full_path = $file["folder_path"]."/".$file["file_name"];
												$playable_class="";
												$color_class="";
												if( 
													$file["extension"] == 'mp4'
												){
													$playable_class="playable";
													$color_class="blue";
												}else{
													$playable_class="unplayable";
													$color_class="grey";
												}

												$json =  htmlspecialchars(json_encode( $file ), ENT_QUOTES, 'UTF-8');
												// print_r($json);

												print_r('<div class="ui mini button '.$color_class.' movie_file '.$playable_class.'" data-file_json=\''.$json."' data-path=\"".$full_path."\">".$file["file_name"]."</div>");
											}
										}
									?>
								</div>
							</td>
							<td>
								<?php
									//3D
									if( is_array($movie['3D']) ){
										foreach ($movie['3D'] as $i_3D => $_3D) {
											print_r($_3D."<br/>");
										}
									}
								?>
							</td>

						</tr>
				<?php
					}
				
				?>
			</tbody>
		</table>

		<div id="recode_ask" class="ui modal">
			<i class="close icon"></i>
			<div class="header">
				This file cannot be played in browser AS IS
			</div>
			<div class="image content">
				<div class="ui medium image">
					<img src="images/movie_reel.png">
				</div>
				<div class="description">
					<div class="ui header">This Movie File needs to be re-coded.</div>
					<p>I can show you some options for getting it to work.</p>
				</div>
			</div>
			<div class="actions">
				<div class="ui black deny button">
					Nope
				</div>
				<div class="ui positive right labeled icon button">
					Show me the Options
					<i class="film icon"></i>
				</div>
			</div>
		</div>

		<div class="page_link">
			<a href="re-index.php">Re-Index Movies</a>
		</div>


		<!-- Load React. -->
		<!-- Note: when deploying, replace "development.js" with "production.min.js". -->
		<script src="https://unpkg.com/react@16/umd/react.development.js" crossorigin></script>
		<script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js" crossorigin></script>

		<!-- Load our React component. -->
		<script src="javascript/react_movie_list.js"></script>
		<script src="javascript/movie_gallery.js"></script>

	</body>
</html>