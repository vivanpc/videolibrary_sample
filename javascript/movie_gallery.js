

$(document).ready(function(){
	$.getJSON("movies.json", function(movie_json){
		console.debug(movie_json);
	});



	//Initialize DataTable
	$('#movie_list_table').DataTable();







	// on filename click PLAYABLE
	$('#movie_list_table').on('click', 'div.movie_file.playable', function(event){
		var div = event.target;
		var src = $(div).data('path');
		console.debug('Play video: '+src);
		console.debug(src);

		movie_gallery.set_video_src(src);

	});

	// on filename click UNPLAYABLE
	$('#movie_list_table').on('click', 'div.movie_file.unplayable', function(event){
		var div_movie_file = event.target;
		var src = $(div_movie_file).data('path');
		console.debug('Play video: '+src);
		console.debug(src);

		
		
		$('#recode_ask').modal(
			{
				onApprove: function(x){
					movie_gallery.go_to_recode_page(div_movie_file);
				}
			}	
		).modal('show');


		


	});

	//Initialize src on reload from localStorage
	if( localStorage.getItem('last_video_src') ){
		movie_gallery.set_video_src(localStorage.getItem('last_video_src'));
	}

});


var movie_gallery = {

	// context: null,

	set_video_src: function(src){
		$("video#video_player")[0].src = src;
		localStorage.setItem('last_video_src', src);
	},

	go_to_recode_page: function(div_movie_file){

		console.debug( div_movie_file );

		var src = $(div_movie_file).data('path');
		var file_json = $(div_movie_file).data('file_json');

		console.debug(file_json);

		var form = $('<form action="movie_recode.php?path='+src+'" method="post">'+
			'<input type="text" name="path" value="' + src + '" />'+
			'<input type="text" name="file_json" value=\'' + JSON.stringify(file_json) + '\' />'+
			'</form>');
		$('body').append(form);
		form.submit();
	}
}
