<?php session_start(); ?>
<!DOCTYPE html>
<?php

	require_once 'movie_master_config.php';

?>
<!--
Declarations
logolink
images link
-->

<html>
<head>

<script src="node_modules\jquery\dist\jquery.min.js"></script>

<!--
▀█▀ █▀▄▀█ █▀▀█ █▀▀█ █▀▀█ ▀▀█▀▀ █▀▀ 
▒█░ █░▀░█ █░░█ █░░█ █▄▄▀ ░░█░░ ▀▀█ 
▄█▄ ▀░░░▀ █▀▀▀ ▀▀▀▀ ▀░▀▀ ░░▀░░ ▀▀▀ 
-->

<!-- standardpane -->
<!--link rel="stylesheet" type="text/css" href="Modules/standardpane/standardpane.css"-->


<!--////////////////////////////////////////////////-->
<link rel="stylesheet" type="text/css" href="css/videokiosk.css">





<title>Video Kiosk: ReIndex</title>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
</head>
<body>

<!-- ----------------------------------------------------------------------------------- -->
	<div id="VideoKiosk_Main" class="" style="border:red 1px solid;">

<?php 

	$mm = new movie_master_config;
	//printHeader();
	// $aliased_movie_folder = 'P:/Movies/'; //physical location of apache httpd.conf aliased movie folder
	// $aliased_movie_folder = new directory_( 'P:/Movies', null, '/p_movies' );
	$aliased_movie_folder = new directory_( $mm->Machine_Root, null, $mm->Aliased_Root );




	function scandir_ignore($dir){
		$content = scandir($dir);
		unset($content[ array_search('.', $content) ]);
		unset($content[ array_search('..', $content) ]);
		return $content;
	}

	$folders = scandir_ignore($aliased_movie_folder->name.'/');

	$movie_master = [];
	$movie_file_extensions = array('mp4','avi','mkv','m4v');

//CLASSES////////////////////
	class movie {
		public $foldername = '';
		public $title = '';
		public $year = '';
		public $files = array();
		public $directory;
		private $details_extracted = false;

		function __construct( $name, $directory, $title='', $year='', $video='', $audio='', $_3D='' ){
			$this->name = $name;
			$this->directory = $directory;
			$this->title = $title;
			$this->year = $year;
			$this->video = $video;
			$this->audio = $audio;
			$this->_3D = $_3D != '' ? [$_3D] : [];

			$this->extract_details_from_name();
		}

		//read, parse and interpret info from folder name
		public function extract_details_from_name(){
			//extract year info
			$pos_open_paren = strrpos($this->name, '(');
			$pos_close_paren = strrpos($this->name, ')');
			if( $pos_open_paren && $pos_close_paren ){
				$this->year = [trim(substr($this->name, $pos_open_paren+1, $pos_close_paren-$pos_open_paren-1 ))];
			}

			//extract video, audio, and 3D info
			$pos_open_bracket_first = strpos($this->name, '[');
			$pos_close_bracket_first = strpos($this->name, ']');

			$pos_open_bracket_last = strrpos($this->name, '[');
			$pos_close_bracket_last = strrpos($this->name, ']');

			if( $pos_open_bracket_first && $pos_close_bracket_first ){
				$bracket_contents = strtolower(trim(substr($this->name, $pos_open_bracket_first+1, $pos_close_bracket_first-$pos_open_bracket_first-1 )));

				if( $pos_open_bracket_first<$pos_open_bracket_last && $pos_close_bracket_first<$pos_open_bracket_last ){
					$bracket_contents .= ' '.strtolower(trim(substr($this->name, $pos_open_bracket_last+1, $pos_close_bracket_last-$pos_open_bracket_last-1 )));
				}
				$this->extract_and_assign_attributes_from_bracket_info($bracket_contents);
			}

			if( count($this->_3D) == 0 && strpos($this->name, '3D') === 0 ){
				array_push( $this->_3D, '3D');
			}

			$this->details_extracted = true;
		}

		//read and interpret bracket info from folder name
		private function extract_and_assign_attributes_from_bracket_info($bracket_contents){
			//video
			$this->video = [];
			if( strpos($bracket_contents, '1080') !== false ){
				array_push( $this->video, '1080');
			}else if( strpos($bracket_contents, '720') !== false ){
				array_push( $this->video, '720');
			}
			if( strpos($bracket_contents, 'low') !== false || strpos($bracket_contents, 'cam') !== false ){
				array_push( $this->video, 'low' );
			}

			//audio
			if( strpos($bracket_contents, '6ch') !== false || strpos($bracket_contents, '5.1') ){
				$this->audio = ['5.1','surround'];
			}else if( strpos($bracket_contents, '2ch') !== false ){
				$this->audio = ['2ch','stereo'];
			}

			//3D
			// $this->_3D = [];
			if( strpos($bracket_contents, 'ou') !== false ){
				array_push( $this->_3D, 'OU');
			}else if( strpos($bracket_contents, 'sbs') !== false ){
				array_push( $this->_3D, 'SBS');
			}else if( strpos($bracket_contents, '3d') !== false ){
				array_push( $this->_3D, '3D');
			}
		}

		public function files_to_array(){
			$files = [];
			foreach( $this->files as $x => $file){
				array_push($files, $file->to_array());
			}
			return $files;
		}

		public function to_array(){
			if( $this->details_extracted != true ){
				$this->extract_details_from_name();
			}
			return array(
				"type" => ['movie'],
				"name" => [$this->name],
				"title" => [$this->title],
				"year" => $this->year,
				"video" => $this->video,
				"audio" => $this->audio,
				"3D" => $this->_3D,
				"files" => $this->files_to_array()
			);

		}

		public function to_json(){
			return json_encode( $this->to_array());
		}
	}


	class directory_ {
		public $name = '';
		public $sub_directories = array();
		public $parent_directory;
		public $client_alias;
		function __construct( $name, $parent_directory, $client_alias=null ){
			$this->name = $name;
			$this->parent_directory = $parent_directory;
			$this->client_alias = $client_alias;
		}

		public function get_path(){
			$path=$this->name;
			$dir = $this->parent_directory;
			
			while ( is_object($dir) ){
				if( $dir->name ){
					$path = $dir->name.'/'.$path;
				}
				$dir=$dir->parent_directory;
			}

			// print_r('PATH::::: '.$path ) ;
			return $path;
		}
		public function get_client_path(){
			$path=$this->name;
			$dir = $this->parent_directory;
			
			while ( is_object($dir) ){
				if( $dir->name || $dir->client_alias ){
					if( $dir->client_alias  ){
						$path = $dir->client_alias.'/'.$path;
					}else{
						$path = $dir->name.'/'.$path;
					}
				}
				$dir=$dir->parent_directory;
			}

			// print_r('PATH::::: '.$path ) ;
			return $path;
		}

		public function jsonSerialize(){
			return get_object_vars($this);
		}

	}

	// movie files in this case only create after determining via is_movie_file()
	class file {
		public $type = '';
		public $file_name = '';
		public $folder_path ='';
		public $parent_directory;
		public $extension = '';

		function __construct( $type, $file_name, $parent_directory ){
			$this->type = $type;
			$this->file_name = $file_name;
			$this->parent_directory = $parent_directory;

			// echo('<pre>');
			// print_r( pathinfo($parent_directory->get_client_path())  );
			// echo('</pre>');

			$p_inf = pathinfo($parent_directory->get_client_path());
			$this->extension = $p_inf['extension'];
			$this->folder_path = $p_inf['dirname'];
		}

		public function get_path(){
			$path=$this->file_name;
			$dir = $this->parent_directory;
			if ( is_object($dir) ){
				$path = $dir->get_path();
			}
			return $path;
		}
		public function get_client_path(){
			$path=$this->file_name;
			$dir = $this->parent_directory;
			if ( is_object($dir) ){
				$path = $dir->get_client_path();
			}
			return $path;
		}

		public function to_array(){
			return array(
				"type" => $this->type,
				"file_name" => $this->file_name,
				"extension" => $this->extension,
				"folder_path" => $this->folder_path
			);
		}

		public function to_json(){
			return json_encode( $this->to_array());
		}
	}
//////////////////////////////


	function is_movie_file($file_name){
		global $movie_file_extensions;
		$pieces = explode(".", $file_name);
		if( in_array( $pieces[ count($pieces)-1] , $movie_file_extensions) ){
			return true;
		}else{
			return false;
		}
	}


	function add_file_or_directory( $movie, $file_or_folder_name, $parent_directory, $iteration=0 ){
		if( is_movie_file($file_or_folder_name) ){
			// echo('<p style="color:green;">FOUND MOVIE FILE::::   '.$file_or_folder_name.'</p>');
			array_push( $movie->files, new file( 'movie', $file_or_folder_name, $parent_directory ) );
		}else if( is_dir( $parent_directory->get_path()) ){
			// echo('<h2>Direcotry</h2>');

			// if( $iteration < 10 ){
				// echo('<p style="color:green;">'.$parent_directory->get_path().'</p>');
				$subs = scandir_ignore( $parent_directory->get_path() );
				foreach( $subs as $x => $sub ){

					$new_dir = new directory_( $sub, $parent_directory );
					array_push( $parent_directory->sub_directories, $new_dir );
					add_file_or_directory( $movie, $sub, $new_dir, $iteration++ );
				}
			// }else{
			// 	echo('<h1>Iterated too much</h1>');
			// }
		}else{
			// echo('<h1>W fucked up!!!! ----'.$parent_directory->get_path().'</h1>');
		}
		// echo('<h2>'.$parent_directory->get_path().'</h2>');
	}

	foreach ( $folders as $x => $folder ){
		$movie_directory = new directory_( $folder, $aliased_movie_folder );
		// echo('<p>create movie object</p>');
		$movie = new movie( $folder, $movie_directory );
		
		add_file_or_directory( $movie, $folder, $movie_directory );//also want to detect file details such as year audio etc - can possibly it by inspecting the files themselves??
		array_push($movie_master, $movie );
	} 

	// $movie_files_json = json_encode($movie_master);
	// echo('<pre>');
	// // print_r($movie_files_json);
	// echo('</pre>');

	// echo('<hr>');

	// echo('<pre>');
	// // print_r( $movie_master );
	// echo('</pre>');
	// echo '<ul id="movie_master_list">';


	$full_movie_list_for_json = [];

	foreach ( $movie_master as $x => $movie ){
		// echo('<li>');
		// echo('<span style="color:blue;padding-left:20px;">'.$movie->name.'</span>');
		// print_r( $movie->to_array() );
		$mj = $movie->to_json();

		array_push( $full_movie_list_for_json,  $movie->to_array() );

		// echo('<pre>');
		// print_r($mj);
		// echo('</pre>');
		// foreach( $movie->files as $x => $file){
		// 	$mf = $file->to_json();
		// 	echo('<pre>');
		// 	print_r($mf);
		// 	echo('</pre>');
		// 	// echo('<p style="padding-left:200px;cursor:pointer;" class="movie_file" data-source="'.$file->get_client_path().'">');
		// 	// print_r( $file->get_client_path() );
		// 	// echo('</p>');
		// }
		// echo('</li>');
	}
	// echo('</ul>');

	$movie_json = json_encode($full_movie_list_for_json);

	file_put_contents( 'movies.json', $movie_json );
	echo('<pre>');
	print_r($movie_json);
	echo('</pre>');
?>



<!-- ------ TEXT AND VIDEO TABLE -------------------------------------------- -->
		<table id="videoandtexttable" style="border:1px solid green">
		<tr class="videoandtext">
			<td class="text">
			<!-- stuff -->
			</td>
			<td class="videoplayer">
				<video id="videoelement" width="640" height="482" controls>
					<source src="" type="video/mp4">
					Your browser does not support the video tag.
				</video> 
			</td>
			<td class="text">
			<!-- stuff -->
			</td>
		</tr>
		</table>
		<!-- <table id="videocontroltable12">
			<tr>
				<td>
					<img class="playvideo" src="" videoname="/p_movies/300/300 (2006) [1080p]/300.2006.BluRay.1080p.x264.YIFY.mp4" />
					300
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG2.png" videoname="videos/DTG2.mp4" />
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG3.png" videoname="videos/DTG3.mp4" />
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG4.png" videoname="videos/DTG4.mp4" />
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG5.png" videoname="videos/DTG5.mp4" />
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG5.png" videoname="videos/DTG5.mp4" />
				</td>
			</tr>
			<tr>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG1.png" videoname="videos/DTG1.mp4" />
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG2.png" videoname="videos/DTG2.mp4" />
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG3.png" videoname="videos/DTG3.mp4" />
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG4.png" videoname="videos/DTG4.mp4" />
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG5.png" videoname="videos/DTG5.mp4" />
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG5.png" videoname="videos/DTG5.mp4" />
				</td>
			</tr>
		</table> -->
<!-- ------ /GALLERY -------------------------------------------- -->

		<div style="float:left;clear:both;">
		</div>
	
	<?php
		//printFooter();
	?>
	</div> <!--VideoKiosk_Main-->

<div class="page_link">
	<a href="movie_gallery.php">Movie Gallery</a>
</div>
<div id="ComponentBay"></div>
<!-- ----------------------------------------------------------------------------------- -->
</body>

<script>

	video = $('#videoelement');
	// write fucntion to read through previewiamge videoname attr and generate array automatically//
	videofiles = ['videos/DTG1.mp4','videos/DTG2.mp4','videos/DTG3.mp4','videos/DTG4.mp4','videos/DTG5.mp4'];
	videoindex = 0;
	$(video).get(0).play();

	//console.debug(videofiles);
	//console.debug(video);
	//console.log( $(video).children('source').attr('src') )

	$('img.playvideo').click(function(){
		//alert( $(this).attr('videoname') );
		playvideo($(this).attr('videoname'));

	});

	//console.debug( $('img.playvideo') );
	function playvideo(videoname){
		$(video).children('source').attr('src',videoname);
		$(video).load();
		$(video).get(0).play();
		console.log("Now Playing: "+videoname);
	}


	$(video).bind("ended", function() {
		currentvideo = $(video).children('source').attr('src');
		videoindex = videofiles.indexOf(currentvideo);
		videoindex++;
		if( videoindex >= videofiles.length){
		videoindex=0;

		}

		playvideo(videofiles[videoindex]);


	});


	$('#movie_master_list').on('click', '.movie_file', function(ev){
		console.debug(ev);

		console.debug(ev.target);

		var source = $(ev.target).data('source');

		console.debug(source);

		// window.playvideo(source)
		var video = $('#videoelement');
		$(video).find('source').attr('src',source);
		$(video)[0].load();
		$(video)[0].play();
	});
		   

</script>
</html>



