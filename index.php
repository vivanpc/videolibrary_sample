<?php session_start(); ?>
<!DOCTYPE html>
<?php
   // include_once 'php/DBAclass.php';

?>
<!--
Declarations
logolink
images link
-->

<html>
<head>

<script src="node_modules\jquery\dist\jquery.min.js"></script>

<!--
▀█▀ █▀▄▀█ █▀▀█ █▀▀█ █▀▀█ ▀▀█▀▀ █▀▀ 
▒█░ █░▀░█ █░░█ █░░█ █▄▄▀ ░░█░░ ▀▀█ 
▄█▄ ▀░░░▀ █▀▀▀ ▀▀▀▀ ▀░▀▀ ░░▀░░ ▀▀▀ 
-->

<!-- standardpane -->
<!--link rel="stylesheet" type="text/css" href="Modules/standardpane/standardpane.css"-->


<!--////////////////////////////////////////////////-->
<link rel="stylesheet" type="text/css" href="css/videokiosk.css">





<title>Video Kiosk</title>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
</head>
<body>

<!-- ----------------------------------------------------------------------------------- -->
	<div id="VideoKiosk_Main" class="" style="border:red 1px solid;">

<?php 


	//printHeader();
	// $aliased_movie_folder = 'P:/Movies/'; //physical location of apache httpd.conf aliased movie folder
	
	$aliased_movie_folder = new directory_( 'P:/Movies', null, '/p_movies' );


	function scandir_ignore($dir){
		$content = scandir($dir);
		unset($content[ array_search('.', $content) ]);
		unset($content[ array_search('..', $content) ]);
		return $content;
	}

	$folders = scandir_ignore($aliased_movie_folder->name.'/');

	$movie_master = [];
	$movie_file_extensions = array('mp4','avi','mkv');

//CLASSES////////////////////
	class movie {
		public $foldername = '';
		public $title = '';
		public $year = '';
		public $files = array();
		public $directory;

		function __construct( $name, $directory, $title='', $year='', $video='', $audio='', $_3D='' ){
			$this->name = $name;
			$this->directory = $directory;
			$this->title = $title;
			$this->year = $year;
			$this->video = $video;
			$this->audio = $audio;
			$this->_3D = $_3D;
		}
	}


	class directory_ {
		public $name = '';
		public $sub_directories = array();
		public $parent_directory;
		public $client_alias;
		function __construct( $name, $parent_directory, $client_alias=null ){
			$this->name = $name;
			$this->parent_directory = $parent_directory;
			$this->client_alias = $client_alias;
		}

		public function get_path(){
			$path=$this->name;
			$dir = $this->parent_directory;
			
			while ( is_object($dir) ){
				if( $dir->name ){
					$path = $dir->name.'/'.$path;
				}
				$dir=$dir->parent_directory;
			}

			// print_r('PATH::::: '.$path ) ;
			return $path;
		}
		public function get_client_path(){
			$path=$this->name;
			$dir = $this->parent_directory;
			
			while ( is_object($dir) ){
				if( $dir->name || $dir->client_alias ){
					if( $dir->client_alias  ){
						$path = $dir->client_alias.'/'.$path;
					}else{
						$path = $dir->name.'/'.$path;
					}
				}
				$dir=$dir->parent_directory;
			}

			// print_r('PATH::::: '.$path ) ;
			return $path;
		}
	}

	// movie files in this case only create after determineing via is_movie_file()
	class file {
		public $type = '';
		public $file_name = '';
		public $parent_directory;

		function __construct( $type, $file_name, $parent_directory ){
			$this->type = $type;
			$this->file_name = $file_name;
			$this->parent_directory = $parent_directory;
		}

		public function get_path(){
			$path=$this->file_name;
			$dir = $this->parent_directory;
			if ( is_object($dir) ){
				$path = $dir->get_path();
			}
			return $path;
		}
		public function get_client_path(){
			$path=$this->file_name;
			$dir = $this->parent_directory;
			if ( is_object($dir) ){
				$path = $dir->get_client_path();
			}
			return $path;
		}
	}
//////////////////////////////


	function is_movie_file($file_name){
		global $movie_file_extensions;
		$pieces = explode(".", $file_name);
		if( in_array( $pieces[ count($pieces)-1] , $movie_file_extensions) ){
			return true;
		}else{
			return false;
		}
	}


	function add_file_or_directory( $movie, $file_or_folder_name, $parent_directory, $iteration=0 ){
		if( is_movie_file($file_or_folder_name) ){
			// echo('<p style="color:green;">FOUND MOVIE FILE::::   '.$file_or_folder_name.'</p>');
			array_push( $movie->files, new file( 'movie', $file_or_folder_name, $parent_directory ) );
		}else if( is_dir( $parent_directory->get_path()) ){
			// echo('<h2>Direcotry</h2>');

			// if( $iteration < 10 ){
				// echo('<p style="color:green;">'.$parent_directory->get_path().'</p>');
				$subs = scandir_ignore( $parent_directory->get_path() );
				foreach( $subs as $x => $sub ){

					$new_dir = new directory_( $sub, $parent_directory );
					array_push( $parent_directory->sub_directories, $new_dir );
					add_file_or_directory( $movie, $sub, $new_dir, $iteration++ );
				}
			// }else{
			// 	echo('<h1>Iterated too much</h1>');
			// }
		}else{
			// echo('<h1>W fucked up!!!! ----'.$parent_directory->get_path().'</h1>');
		}
		// echo('<h2>'.$parent_directory->get_path().'</h2>');
	}

	foreach ( $folders as $x => $folder){
		$movie_directory = new directory_( $folder, $aliased_movie_folder );
		// echo('<p>create movie object</p>');
		$movie = new movie( $folder, $movie_directory );
		
		add_file_or_directory( $movie, $folder, $movie_directory );//also want to detect file details such as year audio etc - can possibly it by inspecting the files themselves??
		array_push($movie_master, $movie );
	} 


	echo '<ul id="movie_master_list">';
	foreach ( $movie_master as $x => $movie ){
		echo('<li>');
		echo('<span style="color:blue;padding-left:20px;">'.$movie->name.'</span>');
		// print_r( $movie );
		foreach( $movie->files as $x => $file){
			echo('<p style="padding-left:200px;cursor:pointer;" class="movie_file" data-source="'.$file->get_client_path().'">');
			print_r( $file->get_client_path() );
			echo('</p>');
		}
		echo('</li>');
	}
	echo('</ul>');
?>



<!-- ------ TEXT AND VIDEO TABLE -------------------------------------------- -->
		<table id="videoandtexttable" style="border:1px solid green">
		<tr class="videoandtext">
			<td class="text">
			<!-- stuff -->
			</td>
			<td class="videoplayer">
				<video id="videoelement" width="640" height="482" controls>
					<source src="/p_movies/300/300 (2006) [1080p]/300.2006.BluRay.1080p.x264.YIFY.mp4" type="video/mp4">
					Your browser does not support the video tag.
				</video> 
			</td>
			<td class="text">
			<!-- stuff -->
			</td>
		</tr>
		</table>
		<!-- <table id="videocontroltable12">
			<tr>
				<td>
					<img class="playvideo" src="" videoname="/p_movies/300/300 (2006) [1080p]/300.2006.BluRay.1080p.x264.YIFY.mp4" />
					300
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG2.png" videoname="videos/DTG2.mp4" />
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG3.png" videoname="videos/DTG3.mp4" />
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG4.png" videoname="videos/DTG4.mp4" />
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG5.png" videoname="videos/DTG5.mp4" />
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG5.png" videoname="videos/DTG5.mp4" />
				</td>
			</tr>
			<tr>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG1.png" videoname="videos/DTG1.mp4" />
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG2.png" videoname="videos/DTG2.mp4" />
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG3.png" videoname="videos/DTG3.mp4" />
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG4.png" videoname="videos/DTG4.mp4" />
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG5.png" videoname="videos/DTG5.mp4" />
				</td>
				<td>
					<img class="playvideo" src="videos/previewimages/DTG5.png" videoname="videos/DTG5.mp4" />
				</td>
			</tr>
		</table> -->
<!-- ------ /GALLERY -------------------------------------------- -->

		<div style="float:left;clear:both;">
		</div>
	
	<?php
		//printFooter();
	?>
	</div> <!--VideoKiosk_Main-->

<div id="ComponentBay"></div>
<!-- ----------------------------------------------------------------------------------- -->
</body>

<script>

	video = $('#videoelement');
	// write fucntion to read through previewiamge videoname attr and generate array automatically//
	videofiles = ['videos/DTG1.mp4','videos/DTG2.mp4','videos/DTG3.mp4','videos/DTG4.mp4','videos/DTG5.mp4'];
	videoindex = 0;
	$(video).get(0).play();

	//console.debug(videofiles);
	//console.debug(video);
	//console.log( $(video).children('source').attr('src') )

	$('img.playvideo').click(function(){
		//alert( $(this).attr('videoname') );
		playvideo($(this).attr('videoname'));

	});

	//console.debug( $('img.playvideo') );
	function playvideo(videoname){
		$(video).children('source').attr('src',videoname);
		$(video).load();
		$(video).get(0).play();
		console.log("Now Playing: "+videoname);
	}


	$(video).bind("ended", function() {
		currentvideo = $(video).children('source').attr('src');
		videoindex = videofiles.indexOf(currentvideo);
		videoindex++;
		if( videoindex >= videofiles.length){
		videoindex=0;

		}

		playvideo(videofiles[videoindex]);


	});


	$('#movie_master_list').on('click', '.movie_file', function(ev){
		console.debug(ev);

		console.debug(ev.target);

		var source = $(ev.target).data('source');

		console.debug(source);

		// window.playvideo(source)
		var video = $('#videoelement');
		$(video).find('source').attr('src',source);
		$(video)[0].load();
		$(video)[0].play();
	});
		   

</script>
</html>



